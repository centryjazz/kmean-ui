import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Sumary } from 'src/app/models/dashboard.model';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private http: HttpClient
  ) { }
  getSumary() {
    return this.http.get<Sumary>('http://localhost:9000/api/dashboard/');
    // console.log('data pegawai');
  }
}
