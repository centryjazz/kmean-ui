import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bidang} from '../../models/bidang.model'
import { StatusProses } from 'src/app/models/updateStatus.model';
@Injectable({
  providedIn: 'root'
})
export class BidangService {

  constructor(private http: HttpClient) { }

  getAllData(){
    return this.http.get('http://localhost:9000/api/bidang/all');
    // console.log('data pegawai');
  }
  getDataByIdBidang(id:number){
    return this.http.get<Bidang>(`http://localhost:9000/api/bidang/${id}`);
  }
  deleteData(id:number){
    return this.http.delete<StatusProses>(`http://localhost:9000/api/bidang/remove/${id}`);
  }

  updateData(id:number,data:Bidang){
    return this.http.put(`http://localhost:9000/api/bidang/edit/${id}`,data);
  }
  addData(data:Bidang){
     return this.http.post(`http://localhost:9000/api/bidang/add`,data); 
  }
}