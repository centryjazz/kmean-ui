import { TestBed } from '@angular/core/testing';

import { PerangkatService } from './perangkat.service';

describe('PerangkatService', () => {
  let service: PerangkatService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PerangkatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
