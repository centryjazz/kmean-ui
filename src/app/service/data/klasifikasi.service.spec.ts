import { TestBed } from '@angular/core/testing';

import { KlasifikasiService } from './klasifikasi.service';

describe('KlasifikasiService', () => {
  let service: KlasifikasiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KlasifikasiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
