import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pegawai } from 'src/app/models/pegawai.model';
import { Bidang } from 'src/app/models/bidang.model';
import { StatusProses } from 'src/app/models/updateStatus.model';

@Injectable({
  providedIn: 'root'
})
export class DataPegawaiService {

 
  constructor(
    private http: HttpClient
  ) { }


  getDataPegawaiFromService(){
    return this.http.get<Pegawai[]>('http://localhost:9000/api/pengguna/all');
    // console.log('data pegawai');
  }
  getSingleData(id:number){
    return this.http.get<Pegawai>(`http://localhost:9000/api/pengguna/${id}`);
  }
  getBidang(id:number){
    return this.http.get<Bidang>(`http://localhost:9000/api/bidang/${id}`);
  }

  deleteData(id:number){
    return this.http.delete<StatusProses>(`http://localhost:9000/api/pengguna/remove/${id}`);
  }

  updateData(id:number,data:Pegawai){
    return this.http.put(`http://localhost:9000/api/pengguna/edit/${id}`,data);
  }
  addData(data:Pegawai){
     return this.http.post<Pegawai>(`http://localhost:9000/api/pengguna/add`,data); 
  }
}
