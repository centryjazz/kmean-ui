import { TestBed } from '@angular/core/testing';

import { PemilikService } from './pemilik.service';

describe('PemilikService', () => {
  let service: PemilikService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PemilikService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
