import { TestBed } from '@angular/core/testing';

import { DataPegawaiService } from './data-pegawai.service';

describe('DataPegawaiService', () => {
  let service: DataPegawaiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataPegawaiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
