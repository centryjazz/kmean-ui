import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Perangkat } from 'src/app/models/perangkat.model';
import { StatusProses } from 'src/app/models/updateStatus.model';

@Injectable({
  providedIn: 'root'
})
export class PerangkatService {

  constructor(
    private http: HttpClient
  ) { }

  getAllData(){
    return this.http.get<Perangkat[]>('http://localhost:9000/api/perangkat/all');
    // console.log('data pegawai');
  }
  getDataByPengguna(id:number){
    return this.http.get<Perangkat>(`http://localhost:9000/api/perangkat/pengguna/${id}`);
  }

  getSinglePerangkat(id:number){
    return this.http.get<Perangkat>(`http://localhost:9000/api/perangkat/${id}`);
  }
  getDataByPerangkatId(id:number){
    // http://localhost:9000/api/perangkat/single/
    return this.http.get<Perangkat>(`http://localhost:9000/api/perangkat/single/${id}`);
  }

  deleteData(id:number){
    return this.http.delete<StatusProses>(`http://localhost:9000/api/perangkat/remove/${id}`);
  }

  updateData(id:number,data:Perangkat){
    return this.http.put(`http://localhost:9000/api/perangkat/edit/${id}`,data);
  }
  addData(data:Perangkat){
     return this.http.post<Perangkat>(`http://localhost:9000/api/perangkat/add`,data); 
  }
  

}
