import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Klasifikasi } from 'src/app/models/klasifikasi.model';
import { StatusProses } from 'src/app/models/updateStatus.model';

@Injectable({
  providedIn: 'root'
})
export class KlasifikasiService {

 
  constructor(private http: HttpClient) { }

  getAllData(){
    return this.http.get<Klasifikasi[]>('http://localhost:9000/api/kmean/data/cluster');
    // console.log('data pegawai');
  }
  // getDataByPerangkat(id:number){
  //   return this.http.get(`http://localhost:9000/api/pemilik/${id}`);
  // }

  getSingleRawData(id:number){
    return this.http.get(`http://localhost:9000/api/kmean/dataset/raw/${id}`);
  }
  updateData(){
    return this.http.get<StatusProses>(`http://localhost:9000/api/kmean/update`);  
  }

}
