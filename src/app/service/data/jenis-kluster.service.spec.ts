import { TestBed } from '@angular/core/testing';

import { JenisKlusterService } from './jenis-kluster.service';

describe('JenisKlusterService', () => {
  let service: JenisKlusterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JenisKlusterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
