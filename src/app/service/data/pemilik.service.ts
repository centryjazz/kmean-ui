import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pemilik } from 'src/app/models/pemilik.model';
import { StatusProses } from 'src/app/models/updateStatus.model';

@Injectable({
  providedIn: 'root'
})
export class PemilikService {

  constructor(private http: HttpClient) { }

  getAllData(){
    return this.http.get('http://localhost:9000/api/pemilik/all');
    // console.log('data pegawai');
  }
  getDataById(id:number){
    return this.http.get<Pemilik>(`http://localhost:9000/api/pemilik/getone/${id}`);
  }
  getDataByPerangkat(id:number){
    return this.http.get<Pemilik>(`http://localhost:9000/api/pemilik/${id}`);
  }

  deleteData(id:number){
    return this.http.delete<StatusProses>(`http://localhost:9000/api/pemilik/remove/${id}`);
  }

  updateData(id:number,data:Pemilik){
    return this.http.put(`http://localhost:9000/api/pemilik/edit/${id}`,data);
  }
  addData(data:Pemilik){
     return this.http.post(`http://localhost:9000/api/pemilik/add`,data); 
  }
}
