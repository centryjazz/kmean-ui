import { TestBed } from '@angular/core/testing';

import { BidangService } from './bidang.service';

describe('BidangService', () => {
  let service: BidangService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BidangService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
