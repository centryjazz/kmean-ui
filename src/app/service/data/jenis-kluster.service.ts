import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { KlusterSpec } from 'src/app/models/kluster.model';
import { Klasifikasi } from 'src/app/models/klasifikasi.model';
import { StatusProses } from 'src/app/models/updateStatus.model';

@Injectable({
  providedIn: 'root'
})
export class JenisKlusterService {

   
  constructor(
    private http: HttpClient
  ) { }


  getAll(){
    return this.http.get('http://localhost:9000/api/kmean/klasifikasi/perangkat');
    // console.log('data pegawai');
  }
  getSigleData(id:number){
    return this.http.get<KlusterSpec>(`http://localhost:9000/api/kmean/klasifikasi/${id}`);
  }
  addDataCluster(kluster : Klasifikasi){
    return this.http.post<Klasifikasi>(`http://localhost:9000/api/kmean/data/cluster/add`,kluster)
  }
  deleteDataCluster(id : number){
    return this.http.delete<StatusProses>(`http://localhost:9000/api/kmean/data/cluster/delete/${id}`)
  }
  // updateDataCluster(id : number){
  //   return this.http.delete<StatusProses>(`http://localhost:9000/api/kmean/data/cluster/delete/${id}`)
  // }
  
  
}
