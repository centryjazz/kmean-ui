import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StatusProses } from '../models/updateStatus.model';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {

  constructor(
    private http : HttpClient
  ) { }
  authenticate(username, password) {
    // console.log()
    if (username === 'admin' && password === 'admin') {
      sessionStorage.setItem('authenticateUser',username);
      return true;
    } else {
      return false;
    }
  }
  isUserLoggedIn(){
    let user = sessionStorage.getItem('authenticateUser');
    return !(user === null);
  }
  
}
