import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';
import { StatusProses } from '../models/updateStatus.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http : HttpClient) { }
  
  getLogin(datalogin:User){
    return this.http.post<StatusProses>(`http://localhost:9000/service/login`,datalogin);
  }
}
