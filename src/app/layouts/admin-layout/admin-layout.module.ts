import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClipboardModule } from 'ngx-clipboard';

import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from '../../pages/tables/tables.component';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import{ DataPegawaiComponent } from '../../pages/data-pegawai/data-pegawai.component';
import { DataTablesModule } from 'angular-datatables';
import { DetailPegawaiComponent } from 'src/app/pages/detail-pegawai/detail-pegawai.component';
import { DataKlasifikasiComponent } from 'src/app/pages/data-klasifikasi/data-klasifikasi.component';
import { JenisKlusterComponent } from 'src/app/pages/jenis-kluster/jenis-kluster.component';
import { DetailKlusteringComponent } from 'src/app/pages/detail-klustering/detail-klustering.component';
import { DataPengelolaAsetComponent } from 'src/app/pages/data-pengelola-aset/data-pengelola-aset.component';
import { DataBidangPekerjaanComponent } from 'src/app/pages/data-bidang-pekerjaan/data-bidang-pekerjaan.component';
import { PengelolaCUDComponent } from 'src/app/pages/pengelola-cud/pengelola-cud.component';
import { BidangCudComponent } from 'src/app/pages/bidang-cud/bidang-cud.component';
import { PegawaiCudComponent } from 'src/app/pages/pegawai-cud/pegawai-cud.component';




// import { ModalModule } from 'ngx-bootstrap/modal';

// import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    ClipboardModule,
    DataTablesModule,
    FormsModule,
    
    // HttpClient
    
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TablesComponent,
    IconsComponent,
    MapsComponent,
    DataPegawaiComponent,
    DetailPegawaiComponent,
    DataKlasifikasiComponent,
    JenisKlusterComponent,
    DetailKlusteringComponent,
    DataPengelolaAsetComponent,
    DataBidangPekerjaanComponent,
    PengelolaCUDComponent,
    BidangCudComponent,
    PegawaiCudComponent,

  ]
})

export class AdminLayoutModule {}
