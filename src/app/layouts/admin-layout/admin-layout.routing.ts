import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from '../../pages/tables/tables.component';
// import { DataPegawaiComponent } from 'src/app/pages/data-pegawai/data-pegawai.component';
import { AuthGuardService } from 'src/app/service/auth-guard.service';
import { DataPegawaiComponent } from 'src/app/pages/data-pegawai/data-pegawai.component';
import { DetailPegawaiComponent } from 'src/app/pages/detail-pegawai/detail-pegawai.component';
import { DataKlasifikasiComponent } from 'src/app/pages/data-klasifikasi/data-klasifikasi.component';
import { JenisKlusterComponent } from 'src/app/pages/jenis-kluster/jenis-kluster.component';
import { DetailKlusteringComponent } from 'src/app/pages/detail-klustering/detail-klustering.component';
import { DataPengelolaAsetComponent } from 'src/app/pages/data-pengelola-aset/data-pengelola-aset.component';
import { DataBidangPekerjaanComponent } from 'src/app/pages/data-bidang-pekerjaan/data-bidang-pekerjaan.component';
import { PengelolaCUDComponent } from 'src/app/pages/pengelola-cud/pengelola-cud.component';
import { BidangCudComponent } from 'src/app/pages/bidang-cud/bidang-cud.component';
import { PegawaiCudComponent } from 'src/app/pages/pegawai-cud/pegawai-cud.component';
// import { LoginComponent } from 'src/app/pages/login/login.component';

export const AdminLayoutRoutes: Routes = [
    {
        path: 'dashboard', component: DashboardComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'user-profile', component: UserProfileComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'tables', component: TablesComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'icons', component: IconsComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'maps', component: MapsComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'data-pegawai', component: DataPegawaiComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'detail/:id', component: DetailPegawaiComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'data-klasifikasi', component: DataKlasifikasiComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'jenis-cluster', component: JenisKlusterComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'detail-cluster/:id/:cluster', component: DetailKlusteringComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'data-pengelola', component: DataPengelolaAsetComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'data-bidang', component: DataBidangPekerjaanComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'pengelola/:option/:id', component: PengelolaCUDComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'bidang/:option/:id', component: BidangCudComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'pegawai/:option/:id', component: PegawaiCudComponent,
        canActivate: [AuthGuardService]
    },
    // {
    //     path: 'pegawai/cud', component: PegawaiCudComponent,
    //     canActivate: [AuthGuardService]
    // },


];
