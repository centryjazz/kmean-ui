import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PemilikService } from 'src/app/service/data/pemilik.service';
import { StatusProses } from 'src/app/models/updateStatus.model';
import { Pemilik } from 'src/app/models/pemilik.model';
import { DataTablesModule } from 'angular-datatables';
import { empty } from 'rxjs';

@Component({
  selector: 'app-pengelola-cud',
  templateUrl: './pengelola-cud.component.html',
  styleUrls: ['./pengelola-cud.component.css']
})
export class PengelolaCUDComponent implements OnInit {

  title = '';
  editStatus = false;
  addStatus = false;
  deleteStatus = false;
  validOption = true;
  deleteFail = false;
  updateFail = false;
  addFail = false;
  pengelola = new Pemilik() ;

  statusProses = new StatusProses();
  option = '';
  id: number;
  data: any;
  constructor(
    private activeRoute: ActivatedRoute,
    private route: Router,
    private service: PemilikService

  ) { }

  ngOnInit(): void {
    this.option = this.activeRoute.snapshot.params['option'];
    this.id = this.activeRoute.snapshot.params['id'];

    if (this.option === 'add') {
      this.title = 'Tambah Data Pengelola';
      this.addStatus = true;
      this.pengelola.nama = '';
      this.pengelola.pengelola = '';
    } else if (this.option === 'edit') {
      this.title = 'Edit Data Pengelola';
      this.service.getDataById(this.id).subscribe(
        response => {
          // console.log(response);/
          this.pengelola = response;
          this.editStatus = true;
        }
      );
    } else if (this.option === 'delete') {
      this.title = 'Hapus Data Pengelola';
      this.service.getDataById(this.id).subscribe(
        response => {
          // console.log(response);
          this.data = response;
          this.deleteStatus = true;
        }
      );

    }
    else {
      this.route.navigate(['dashboard']);
    }
  }
  deleteData(id: number) {
    this.service.deleteData(this.id).subscribe(
      response => {
        this.statusProses = response;
        // console.log(this.statusProses);
        if (this.statusProses.Status === true) {
          // console.log('EXECUTE ROUTER LINK');
          this.route.navigate(['data-pengelola']);
        } else {
          this.deleteFail = true;
        }
        // this.deleteResponse = true;

      }
    );
  }
  updateData(id: number) {
    // console.log('DATA RETRIVE :');
    // console.log(this.pengelola);
    if(this.validation(this.pengelola)){
      this.service.updateData(this.id,this.pengelola).subscribe(
        response => {
          this.data = response;
            this.route.navigate(['data-pengelola']);
        }
      );
    }else{
      this.updateFail = true;
    }
    


  }
  addData(){
    // console.log('DATA RETRIVE :');
    // console.log(this.pengelola);
    this.pengelola.id = -1;
    if(this.validation(this.pengelola)){
      this.service.addData(this.pengelola).subscribe(
        response => {
          this.data = response;
            this.route.navigate(['data-pengelola']);
          }
      );
    }else{
        this.addFail = true;
    }
    

  }

  validation(data:Pemilik){
    if(data.nama === ''){
        return false;
    }else if(data.pengelola === ''){
      return false;
    }else{
      return true;
    }
    
  }
  back() {
    this.route.navigate(['data-pengelola']);
  }
}
