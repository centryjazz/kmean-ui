import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengelolaCUDComponent } from './pengelola-cud.component';

describe('PengelolaCUDComponent', () => {
  let component: PengelolaCUDComponent;
  let fixture: ComponentFixture<PengelolaCUDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengelolaCUDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengelolaCUDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
