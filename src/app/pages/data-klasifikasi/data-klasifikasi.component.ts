import { Component, OnInit, AfterViewInit } from '@angular/core';
import { KlasifikasiService } from 'src/app/service/data/klasifikasi.service';
import { PerangkatService } from 'src/app/service/data/perangkat.service';
import { DataPegawaiService } from 'src/app/service/data/data-pegawai.service';
import { DataKlasifikasi } from 'src/app/models/data-klasifikasi.model';
import { Perangkat } from 'src/app/models/perangkat.model';
import { Pegawai } from 'src/app/models/pegawai.model';
import { Klasifikasi } from 'src/app/models/klasifikasi.model';
import { Router } from '@angular/router';
import { StatusProses } from 'src/app/models/updateStatus.model';

@Component({
  selector: 'app-data-klasifikasi',
  templateUrl: './data-klasifikasi.component.html',
  styleUrls: ['./data-klasifikasi.component.css']
})
export class DataKlasifikasiComponent implements OnInit, AfterViewInit {
  dtOptions: DataTables.Settings = {};
  constructor(
    private router: Router,
    private klasifikasiService: KlasifikasiService,
    private perangkatService: PerangkatService,
    private penggunaService: DataPegawaiService
  ) { }
  klasifikasis: Klasifikasi[] = [];
  perangkats: Perangkat[] = [];
  penggunas: Pegawai[] = [];
  responseStatus = false;
  doneResponse = false;
  updateStatus = new StatusProses();
  updateResponseStatus = false;
  dataKlasifikasis: DataKlasifikasi[] = [];
  onUpdate = false;
  updatefailed = false;
  ngOnInit(): void {
    this.getData();
    // this.compareData();
  }
  ngAfterViewInit() {
    this.responseStatus = false;
  }
  getData() {
    // get perangkat data
    this.perangkatService.getAllData().subscribe(
      response => {
        // console.log(response);
        this.perangkats = response;
        //get dpengguna data
        this.penggunaService.getDataPegawaiFromService().subscribe(
          response => {
            // console.log(response);
            this.penggunas = response;
            //get klustering data
            this.klasifikasiService.getAllData().subscribe(
              response => {
                // console.log(response);
                this.klasifikasis = response;
                this.doneResponse = true;
                this.dtOptions = {
                  pagingType: 'full_numbers'
                };
                this.compareData();
              }
            );

          }
        );
      }
    );
  }

  compareData() {

    if (this.doneResponse === true) {


      for (let index = 0; index < this.perangkats.length; index++) {
        var data = new DataKlasifikasi();
        data.idPerangkat = this.perangkats[index].id;
        data.jenis = this.perangkats[index].jenis;
        data.merk = this.perangkats[index].merk;
        data.type = this.perangkats[index].type;
        data.curentCluster = this.klasifikasis[index].curent_cluster;
        data.recomendedCluster = this.klasifikasis[index].recomended_cluster;
        data.pengguna = this.penggunas[index].nama;
        this.dataKlasifikasis.push(data);
      }
      this.responseStatus = true;
      this.onUpdate = false;
      // console.log('data klasifikasi');
      // console.log(this.dataKlasifikasis[1]);
      // console.log(this.dataKlasifikasis[1]);
    }

  }

  
  getStatus(curent: number, recomended: number) {
    if (curent === recomended) {
      return 2;
    } else if (curent < recomended) {
      return 1;
    } else {
      return 0;
    }
  }
  // resetStatus(i:number){
  //   if(){

  //   }
  //   this.responseStatus = false;
  // }
  getDetail() {
    this.router.navigate(['jenis-cluster']);
    // this.router.navigate(['data-pegawai']);
  }
  getDetailData(id: number, idCluster: number) {
    // console.log('id = '+id);
    // console.log('id cluster = '+idCluster)
    this.router.navigate(['detail-cluster', id, idCluster]);
  }

  
  getUpdate(){

    this.responseStatus = false;
    this.onUpdate = true;
    this.klasifikasiService.updateData().subscribe(
      response => {
        // console.log(response);
        this.updateStatus = response;
        this.updateResponseStatus = true;
        if(this.updateResponseStatus === true){
          if(this.updateStatus.Status === true){
            location.reload();
                
          }else{
            this.updatefailed = true;
          }
      }

      }
    );

    

  }
  tampilDataSebelumnya(){
    this.getData();
    this.updatefailed = false;
  }
}
