import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataKlasifikasiComponent } from './data-klasifikasi.component';

describe('DataKlasifikasiComponent', () => {
  let component: DataKlasifikasiComponent;
  let fixture: ComponentFixture<DataKlasifikasiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataKlasifikasiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataKlasifikasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
