import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticateService } from 'src/app/service/authenticate.service';
import { User } from 'src/app/models/user.model';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/service/login.service';
import { StatusProses } from 'src/app/models/updateStatus.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  constructor(private route: Router, private loginService: LoginService) { }
  User = new User();
  invalidLogin = false;
  status = new StatusProses();
  ngOnInit() {
    // let user = sessionStorage.getItem('authenticateUser');
    // if(user !== null){
    //   this.route.navigate(['dashboard']);
    // }else{
    //   this.route.navigate(['login']);
    // }
  }
  ngOnDestroy() {
  }

  login() {

    this.User.id = 0;
    this.User.token = '';
    this.loginService.getLogin(this.User).subscribe(
      response => {
        // console.log(response);
        this.status = response;

        if (this.status.Status === true) {
          // console.log('execute')
          sessionStorage.setItem('authenticateUser',this.User.username);
          this.route.navigate(['dashboard']);
          // this.invalidLogin = false;
        } else {
          this.invalidLogin = true;
        }

        // console.log(this.User.username);
        // console.log(this.User.password);
      }
    );

    // this.perangkatService.getSinglePerangkat(idPerangkat).subscribe(

  }

}
