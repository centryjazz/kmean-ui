import { Component, OnInit } from '@angular/core';
import { JenisKlusterService } from 'src/app/service/data/jenis-kluster.service';
import { Router } from '@angular/router';
import { KlusterSpec } from 'src/app/models/kluster.model';

@Component({
  selector: 'app-jenis-kluster',
  templateUrl: './jenis-kluster.component.html',
  styleUrls: ['./jenis-kluster.component.css']
})
export class JenisKlusterComponent implements OnInit {
  klusters : any;
  responseStatus = false;
  detailStatus = false;
  sigleCluster = new KlusterSpec();
  constructor(
    private jeniClusterService : JenisKlusterService,
    private router : Router
 
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.jeniClusterService.getAll().subscribe(
      response => {
        // console.log(response);
        this.klusters = response;
        this.responseStatus = true;
      }
    );
  }
  getDetail(id:number){
    this.jeniClusterService.getSigleData(id).subscribe(
      response => {
        // console.log(response);
        this.sigleCluster = response;
        this.detailStatus = true;
      }
    );
  }
  back(){
      this.router.navigate(['data-klasifikasi']);
  }
  hide(){
    this.detailStatus = false;
  }
}


