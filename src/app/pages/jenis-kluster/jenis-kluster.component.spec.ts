import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JenisKlusterComponent } from './jenis-kluster.component';

describe('JenisKlusterComponent', () => {
  let component: JenisKlusterComponent;
  let fixture: ComponentFixture<JenisKlusterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JenisKlusterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JenisKlusterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
