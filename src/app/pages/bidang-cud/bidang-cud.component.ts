import { Component, OnInit } from '@angular/core';
import { BidangService } from 'src/app/service/data/bidang.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Bidang } from 'src/app/models/bidang.model';
import { StatusProses } from 'src/app/models/updateStatus.model';

@Component({
  selector: 'app-bidang-cud',
  templateUrl: './bidang-cud.component.html',
  styleUrls: ['./bidang-cud.component.css']
})
export class BidangCudComponent implements OnInit {

  title = '';
  editStatus = false;
  addStatus = false;
  deleteStatus = false;
  validOption = true;
  deleteFail = false;
  updateFail = false;
  addFail = false;
  bidang = new Bidang() ;
  editFail = false;
  statusProses = new StatusProses();
  option = '';
  id: number;
  data: any;
  constructor(
    private activeRoute: ActivatedRoute,
    private route: Router,
    private service: BidangService

  ) { }

  ngOnInit(): void {
    this.option = this.activeRoute.snapshot.params['option'];
    this.id = this.activeRoute.snapshot.params['id'];

    if (this.option === 'add') {
      this.title = 'Tambah Data Bidang';
      this.addStatus = true;
      // init null data
      this.bidang.nama_bidang = '';
    } else if (this.option === 'edit') {
      this.title = 'Edit Data Bidang';
      this.service.getDataByIdBidang(this.id).subscribe(
        response => {
          // console.log(response);/
          this.bidang = response;
          this.editStatus = true;
        }
      );
    } else if (this.option === 'delete') {
      this.title = 'Hapus Data Bidang';
      this.service.getDataByIdBidang(this.id).subscribe(
        response => {
          // console.log(response);
          this.bidang = response;
          this.deleteStatus = true;
        }
      );

    }
    else {
      this.route.navigate(['dashboard']);
    }
  }

  deleteData(id: number) {
    this.service.deleteData(this.id).subscribe(
      response => {
        this.statusProses = response;
        // console.log(this.statusProses);
        if (this.statusProses.Status === true) {
          // console.log('EXECUTE ROUTER LINK');
          this.route.navigate(['data-bidang']);
        } else {
          this.deleteFail = true;
        }
        // this.deleteResponse = true;

      }
    );
  }
  updateData(id: number) {
    // console.log('DATA RETRIVE :');
    // console.log(this.pengelola);
    if(this.validation(this.bidang)){
      this.service.updateData(this.id,this.bidang).subscribe(
        response => {
          this.data = response;
            this.route.navigate(['data-bidang']);
        }
      );
    }else{
      this.editFail = true;
    }
    


  }
  addData(){
    // console.log('DATA RETRIVE :');
    // console.log(this.pengelola);
    this.bidang.id = -1;
    if(this.validation(this.bidang)){
      this.service.addData(this.bidang).subscribe(
        response => {
          this.data = response;
            this.route.navigate(['data-bidang']);
          }
      );
    }else{
        this.addFail = true;
    }
    

  }

  validation(data:Bidang){
    if(data.nama_bidang === ''){
        return false;
    }else{
      return true;
    }
    
  }
  back() {
    this.route.navigate(['data-bidang']);
  }


}
