import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidangCudComponent } from './bidang-cud.component';

describe('BidangCudComponent', () => {
  let component: BidangCudComponent;
  let fixture: ComponentFixture<BidangCudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidangCudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidangCudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
