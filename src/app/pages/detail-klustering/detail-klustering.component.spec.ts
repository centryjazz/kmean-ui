import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailKlusteringComponent } from './detail-klustering.component';

describe('DetailKlusteringComponent', () => {
  let component: DetailKlusteringComponent;
  let fixture: ComponentFixture<DetailKlusteringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailKlusteringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailKlusteringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
