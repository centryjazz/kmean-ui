import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JenisKlusterService } from 'src/app/service/data/jenis-kluster.service';
import { KlasifikasiService } from 'src/app/service/data/klasifikasi.service';

@Component({
  selector: 'app-detail-klustering',
  templateUrl: './detail-klustering.component.html',
  styleUrls: ['./detail-klustering.component.css']
})
export class DetailKlusteringComponent implements OnInit {
  datas: any;
  cluster: any;
  responseStatus1 = false;
  responseStatus2 = false;
  responseStatus3 = false;
  responseStatus0 = false;
  //============
  btnDetail = 'Show';
  btnDetailType = 'btn-success';

  btnDetailSpec = 'Show';
  btnDetailSpecType = 'btn-success';

  btnDetailPerangkat = 'Show';
  btnDetailPerangkatType = 'btn-success';
  //=============


  constructor(
    private router: Router,
    private jenisKluster: JenisKlusterService,
    private service: KlasifikasiService,
    private routeActive: ActivatedRoute
  ) { }
  id: any;
  idcluster: any;

  ngOnInit(): void {
    this.id = this.routeActive.snapshot.params['id'];
    this.idcluster = this.routeActive.snapshot.params['cluster'];
    this.getData();
    // console.log('id = '+ this.id);
    // console.log('id cluster = '+this.idcluster);
  }

  getData() {
    // console.log(this.service.getSingleData(this.id));
    this.service.getSingleRawData(this.id).subscribe(
      response => {
        // console.log(response);
        this.datas = response;
        this.responseStatus0 = true;

        // console.log(this.service.getBidang(this.pegawai.bidang));
        this.jenisKluster.getSigleData(this.idcluster).subscribe(
          response => {
            // console.log(response);
            this.cluster = response;
            // this.responseStatus1 = true;
          }
        );

      }
    );


  }

  back() {
    this.router.navigate(['data-klasifikasi']);
  }
  btnDetailAct(){
    if(this.responseStatus1 === true){
      this.btnDetail = 'Show';
      this.btnDetailType = 'btn-success';
      this.responseStatus1 = false;
      
    }else{
      this.btnDetail = 'Hide';
      this.btnDetailType = 'btn-primary';
      this.responseStatus1 = true;
    }
    
  }

  btnDetailSpecAct(){
    if(this.responseStatus2 === true){
      this.btnDetailSpec = 'Show';
      this.btnDetailSpecType = 'btn-success';
      this.responseStatus2 = false;
      
    }else{
      this.btnDetailSpec = 'Hide';
      this.btnDetailSpecType = 'btn-primary';
      this.responseStatus2 = true;
    }
    
  }

  btnDetailPerangkatAct(){
    if(this.responseStatus3 === true){
      this.btnDetailPerangkat = 'Show';
      this.btnDetailPerangkatType = 'btn-success';
      this.responseStatus3 = false;
      
    }else{
      this.btnDetailPerangkat = 'Hide';
      this.btnDetailPerangkatType = 'btn-primary';
      this.responseStatus3 = true;
    }
    
  }

}




