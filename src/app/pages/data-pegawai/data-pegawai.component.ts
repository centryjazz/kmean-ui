import { Component, OnInit, ɵCodegenComponentFactoryResolver } from '@angular/core';
import { DataPegawaiService } from 'src/app/service/data/data-pegawai.service';
// import { Pegawai } from 'src/app/models/pegawai.model';
// import { Subject } from 'rxjs';
// import { HttpClient } from '@angular/common/http';
// import { NgbModalModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-data-pegawai',
  templateUrl: './data-pegawai.component.html',
  styleUrls: ['./data-pegawai.component.scss']
})
export class DataPegawaiComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  // dtTrigger: Subject = new Subject();
  Pegawais : any;
  constructor(
    private service : DataPegawaiService,
    //  private http : HttpClient,
     private router : Router
     ) { }
  responseVAL = false;
  ngOnInit(): void {

    this.getData();
    
  }

  getData(){
    // console.log(this.service.getDataPegawaiFromService());
    this.service.getDataPegawaiFromService().subscribe(
        response => {
          // console.log(response);
          this.Pegawais = response;
          this.responseVAL = true;
          this.dtOptions = {
            pagingType: 'full_numbers'
          };
          
          // this.dtTrigger.next();
        }
    );

    
  }
handleReponse(response){
  console.log(response);
}

ngOnDestroy(): void {
  // Do not forget to unsubscribe the event
  // this.dtTrigger.unsubscribe();
}

private extractData(res: Response) {
  // const body = res.json();
  // return body.data || {};
}
addData(){
  this.router.navigate(['pegawai','add',0])
}
editData(id:number){
  this.router.navigate(['pegawai','edit',id])
}
deleteData(id:number){
  this.router.navigate(['pegawai','delete',id])
}

getDetail(id : number){
    this.router.navigate(['detail',id])
}
}