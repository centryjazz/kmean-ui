import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataPegawaiService } from 'src/app/service/data/data-pegawai.service';
import { Pegawai } from 'src/app/models/pegawai.model';
import { Bidang } from 'src/app/models/bidang.model';
import { PerangkatService } from 'src/app/service/data/perangkat.service';
import { Perangkat } from 'src/app/models/perangkat.model';
import { PemilikService } from 'src/app/service/data/pemilik.service';
import { Pemilik } from 'src/app/models/pemilik.model';

@Component({
  selector: 'app-detail-pegawai',
  templateUrl: './detail-pegawai.component.html',
  styleUrls: ['./detail-pegawai.component.css']
})
export class DetailPegawaiComponent implements OnInit {

  pegawai = new Pegawai();
  bidang = new Bidang();
  perangkats : any;
  perangkat = new Perangkat();
  pemilik = new Pemilik();
  responseVal = false;
  getDetailForPC = false;
  perangkatResponseStatus = false;
  constructor(
    private router: Router,
    private routeActive : ActivatedRoute,
    private service : DataPegawaiService,
    private perangkatService : PerangkatService,
    private pemilikService : PemilikService
    ) { }
  id : any;

  ngOnInit(): void {
    this.id = this.routeActive.snapshot.params['id'];
    // console.log('ID yang dicari '+ this.id);
    this.getData();
  }

  getData(){
    // console.log(this.service.getSingleData(this.id));
    this.service.getSingleData(this.id).subscribe(
        response => {
          // console.log(response);
          this.pegawai = response;
          
          
          // console.log(this.service.getBidang(this.pegawai.bidang));
          this.service.getBidang(this.pegawai.bidang).subscribe(
            response => {
              // console.log(response);
              this.bidang = response;
              
            }
        ); 
        // console.log(this.perangkatService.getDataByPengguna(this.pegawai.id));
        this.perangkatService.getDataByPengguna(this.pegawai.id).subscribe(
          response => {
            // console.log(response);
            this.perangkats = response;
            this.perangkatResponseStatus = true;
            
          }
        );
        }
    );
    
   
  }
  back(){
    this.router.navigate(['data-pegawai']);
  }
  getDetailPC(idPerangkat:number){
    this.perangkatService.getSinglePerangkat(idPerangkat).subscribe(
      response => {
        // console.log(response);
        this.perangkat = response;
        this.getDetailForPC = true;
        
        // get kepemilikan perangkat
        this.pemilikService.getDataByPerangkat(this.perangkat.idOwner).subscribe(
          response => {
            // console.log(response);
            this.pemilik = response;         
          }
        );

      }
    );
  }


  hide(){
    this.getDetailForPC = false;
  }
}
