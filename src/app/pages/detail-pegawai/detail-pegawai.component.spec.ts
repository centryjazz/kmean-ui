import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPegawaiComponent } from './detail-pegawai.component';

describe('DetailPegawaiComponent', () => {
  let component: DetailPegawaiComponent;
  let fixture: ComponentFixture<DetailPegawaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPegawaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPegawaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
