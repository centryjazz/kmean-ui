import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataPengelolaAsetComponent } from './data-pengelola-aset.component';

describe('DataPengelolaAsetComponent', () => {
  let component: DataPengelolaAsetComponent;
  let fixture: ComponentFixture<DataPengelolaAsetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataPengelolaAsetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataPengelolaAsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
