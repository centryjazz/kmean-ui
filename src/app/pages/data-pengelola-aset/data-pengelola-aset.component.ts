import { Component, OnInit } from '@angular/core';
import { PemilikService } from 'src/app/service/data/pemilik.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-data-pengelola-aset',
  templateUrl: './data-pengelola-aset.component.html',
  styleUrls: ['./data-pengelola-aset.component.css']
})
export class DataPengelolaAsetComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  // dtTrigger: Subject = new Subject();
  pengelola : any;
  constructor(
    private service : PemilikService,
    //  private http : HttpClient,
     private router : Router
     ) { }
  responseVAL = false;
  ngOnInit(): void {

    this.getData();
    
  }

  getData(){
    // console.log(this.service.getDataPegawaiFromService());
    this.service.getAllData().subscribe(
        response => {
          // console.log(response);
          this.pengelola = response;
          this.responseVAL = true;
          this.dtOptions = {
            pagingType: 'full_numbers'
          };
          
          // this.dtTrigger.next();
        }
    );

    
  }
  tambahData(){
    this.router.navigate(['pengelola','add',-1]);
  }
  editData(id:number){
    this.router.navigate(['pengelola','edit',id]);
  }
  deleteData(id:number){
    this.router.navigate(['pengelola','delete',id]);
  }


}
