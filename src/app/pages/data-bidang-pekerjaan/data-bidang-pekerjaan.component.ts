import { Component, OnInit } from '@angular/core';
import { BidangService } from 'src/app/service/data/bidang.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-data-bidang-pekerjaan',
  templateUrl: './data-bidang-pekerjaan.component.html',
  styleUrls: ['./data-bidang-pekerjaan.component.css']
})
export class DataBidangPekerjaanComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  // dtTrigger: Subject = new Subject();
  bidang : any;
  constructor(
    private service : BidangService,
    //  private http : HttpClient,
     private router : Router
     ) { }
  responseVAL = false;
  ngOnInit(): void {

    this.getData();
    
  }

  getData(){
    // console.log(this.service.getDataPegawaiFromService());
    this.service.getAllData().subscribe(
        response => {
          // console.log(response);
          this.bidang = response;
          this.responseVAL = true;
          this.dtOptions = {
            pagingType: 'full_numbers'
          };
          
          // this.dtTrigger.next();
        }
    );

    
  }

  addData(){
    this.router.navigate(['bidang','add',-1]);
  }
  editData(id:number){
    this.router.navigate(['bidang','edit',id]);
  }
  deleteData(id:number){
    this.router.navigate(['bidang','delete',id]);
  }

}
