import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataBidangPekerjaanComponent } from './data-bidang-pekerjaan.component';

describe('DataBidangPekerjaanComponent', () => {
  let component: DataBidangPekerjaanComponent;
  let fixture: ComponentFixture<DataBidangPekerjaanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataBidangPekerjaanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataBidangPekerjaanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
