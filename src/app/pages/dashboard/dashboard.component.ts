import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';

// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2
} from "../../variables/charts";
import { AuthenticateService } from 'src/app/service/authenticate.service';
import { Router } from '@angular/router';
import { DashboardService } from 'src/app/service/data/dashboard.service';
import { Sumary } from 'src/app/models/dashboard.model';
import { DatePipe } from '@angular/common';
import { PerangkatService } from 'src/app/service/data/perangkat.service';
import { KlasifikasiService } from 'src/app/service/data/klasifikasi.service';
import { DataPegawaiService } from 'src/app/service/data/data-pegawai.service';
import { DataKlasifikasi } from 'src/app/models/data-klasifikasi.model';
import { StatusProses } from 'src/app/models/updateStatus.model';
import { Pegawai } from 'src/app/models/pegawai.model';
import { Perangkat } from 'src/app/models/perangkat.model';
import { Klasifikasi } from 'src/app/models/klasifikasi.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  // isUserLogin: boolean = false;

  public data = new Sumary();
  afterStyle1 = 'text-danger';
  afterStyle2 = 'fas fa-arrow-down';

  currentDate = new Date();
  klasifikasis: Klasifikasi[] = [];
  perangkats: Perangkat[] = [];
  penggunas: Pegawai[] = [];
  responseStatus = false;
  doneResponse = false;
  updateStatus = new StatusProses();
  updateResponseStatus = false;
  dataKlasifikasis: DataKlasifikasi[] = [];
  onUpdate = false;
  updatefailed = false;

constructor(
  private router: Router,
  private service : DashboardService,
  private klasifikasiService: KlasifikasiService,
    private perangkatService: PerangkatService,
    private penggunaService: DataPegawaiService

) {

 }

ngOnInit() {
  this.getSumary();
  this.getData();
}

getSumary(){
  this.service.getSumary().subscribe(
    response => {
      // console.log(response);
      this.data = response;
    }
  );
}

getOption(before : number, after: number){
  if (before > after) {
    this.afterStyle1 = 'text-danger';
    this.afterStyle2 = 'fas fa-arrow-down';
  } else if (before === after) {
    this.afterStyle1 = 'text-primary';
    this.afterStyle2 = 'ni ni-fat-delete';
  }
}

getData() {
  // get perangkat data
  this.perangkatService.getAllData().subscribe(
    response => {
      // console.log(response);
      this.perangkats = response;
      //get dpengguna data
      this.penggunaService.getDataPegawaiFromService().subscribe(
        response => {
          // console.log(response);
          this.penggunas = response;
          //get klustering data
          this.klasifikasiService.getAllData().subscribe(
            response => {
              // console.log(response);
              this.klasifikasis = response;
              this.doneResponse = true;
              this.compareData();
            }
          );

        }
      );
    }
  );
}

compareData() {

  if (this.doneResponse === true) {


    for (let index = 0; index < 10; index++) {
      var data = new DataKlasifikasi();
      data.idPerangkat = this.perangkats[index].id;
      data.jenis = this.perangkats[index].jenis;
      data.merk = this.perangkats[index].merk;
      data.type = this.perangkats[index].type;
      data.curentCluster = this.klasifikasis[index].curent_cluster;
      data.recomendedCluster = this.klasifikasis[index].recomended_cluster;
      data.pengguna = this.penggunas[index].nama;
      this.dataKlasifikasis.push(data);
    }
    this.responseStatus = true;
    this.onUpdate = false;
    // console.log('data klasifikasi');
    // console.log(this.dataKlasifikasis[1]);
    // console.log(this.dataKlasifikasis[1]);
  }

}

getStatus(curent: number, recomended: number) {
  if (curent === recomended) {
    return 2;
  } else if (curent < recomended) {
    return 1;
  } else {
    return 0;
  }
}

navigateToKlasifikasi(){
  this.router.navigate(['data-klasifikasi']);
}
}
