import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PegawaiCudComponent } from './pegawai-cud.component';

describe('PegawaiCudComponent', () => {
  let component: PegawaiCudComponent;
  let fixture: ComponentFixture<PegawaiCudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PegawaiCudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PegawaiCudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
