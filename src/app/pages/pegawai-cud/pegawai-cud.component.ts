import { Component, OnInit } from '@angular/core';
import { Pegawai } from 'src/app/models/pegawai.model';
import { StatusProses } from 'src/app/models/updateStatus.model';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { BidangService } from 'src/app/service/data/bidang.service';
import { DataPegawaiService } from 'src/app/service/data/data-pegawai.service';
import { Bidang } from 'src/app/models/bidang.model';
import { Perangkat } from 'src/app/models/perangkat.model';
import { PemilikService } from 'src/app/service/data/pemilik.service';
import { PerangkatService } from 'src/app/service/data/perangkat.service';
import { Pemilik } from 'src/app/models/pemilik.model';
import { Klasifikasi } from 'src/app/models/klasifikasi.model';
import { JenisKlusterService } from 'src/app/service/data/jenis-kluster.service';

@Component({
  selector: 'app-pegawai-cud',
  templateUrl: './pegawai-cud.component.html',
  styleUrls: ['./pegawai-cud.component.css']
})
export class PegawaiCudComponent implements OnInit {

  title = '';
  editStatus = false;
  addStatus = false;
  deleteStatus = false;
  validOption = true;
  deleteFail = false;
  updateFail = false;
  addFail = false;

  editFail = false;
  statusProses = new StatusProses();
  option = '';
  id: number;
  data: any;
  // ----------------------------------------
  selectBidang = false;
  bidangName = '';
  pegawai: Pegawai = new Pegawai();
  perangkat = new Perangkat();
  bidang = new Bidang();
  pemilik = new Pemilik();
  kluster = new Klasifikasi();
  bidangs: any;
  pengelolas: any;
  dtOptionsBidang: DataTables.Settings = {};
  dtOptionsPengelola: DataTables.Settings = {};


  showPerangkat = false;
  perangkatTitle = 'Show';
  perangkatBtn = 'btn-success';

  showPengelola = false;
  pengelolaTitle = 'Show';
  pengelolaBtn = 'btn-success';

  selectPengelola = false;
  owner = '';
  pengelola = '';

  invalidMessage = '';

  //-----------------------------------------




  constructor(
    private activeRoute: ActivatedRoute,
    private route: Router,
    private service: DataPegawaiService,
    private bidangService: BidangService,
    private pengelolaService: PemilikService,
    private perangkatService: PerangkatService,
    private klusterService: JenisKlusterService

  ) { }

  ngOnInit(): void {
    this.option = this.activeRoute.snapshot.params['option'];
    this.id = this.activeRoute.snapshot.params['id'];
    console.log('Pegawai init Data');
    // console.log(this.pegawai)
    this.initDataBidang();
    // this.initPerangkat();
    this.initDataPengelola();

    if (this.option === 'add') {
      this.title = 'Tambah Data Pegawai';
      this.addStatus = true;
      // init data for pegawai
      this.initPenguna();
      this.initPerangkat();
      this.initDataKluster();
    } else if (this.option === 'edit') {
      this.title = 'Edit Data Pegawai';
      // this.initPenguna();
      // this.initPerangkat();
      // this.initDataKluster();

      console.log('id pegawai yang dicari = ' + this.id)
      this.service.getSingleData(this.id).subscribe(
        response => {
          this.pegawai = response;
          // console.log(this.pegawai)

          this.perangkatService.getDataByPerangkatId(this.id).subscribe(
            responseDataPerangkat => {
              this.perangkat = responseDataPerangkat;
              // console.log(this.perangkat)
              this.bidangService.getDataByIdBidang(this.pegawai.bidang).subscribe(
                responseDataBidang => {
                  this.bidang = responseDataBidang;
                  // console.log(this.bidang);
                  this.bidangName = this.bidang.nama_bidang;

                  this.pengelolaService.getDataById(this.perangkat.idOwner).subscribe(
                    response => {
                      this.owner = response.nama;
                      this.pengelola = response.pengelola;
                    }
                  );
                  this.editStatus = true;
                }
              );
            }
          );
        }
      );

    } else if (this.option === 'delete') {
      this.title = 'Hapus Data Pegawai';
      // console.log('id pegawai yang dicari = ' + this.id)
      this.service.getSingleData(this.id).subscribe(
        response => {
          this.pegawai = response;
          // console.log(this.pegawai)

          this.perangkatService.getDataByPerangkatId(this.id).subscribe(
            responseDataPerangkat => {
              this.perangkat = responseDataPerangkat;
              // console.log(this.perangkat)
              this.bidangService.getDataByIdBidang(this.pegawai.bidang).subscribe(
                responseDataBidang => {
                  this.bidang = responseDataBidang;
                  // console.log(this.bidang);
                  this.bidangName = this.bidang.nama_bidang;

                  this.pengelolaService.getDataById(this.perangkat.idOwner).subscribe(
                    response => {
                      this.owner = response.nama;
                      this.pengelola = response.pengelola;
                    }
                  );
                  this.deleteStatus= true;
                }
              );
            }
          );
        }
      );
      
    }
    else {
      this.route.navigate(['dashboard']);
    }
  }

  initPerangkat() {
    this.perangkat.id = -1;
    this.perangkat.jenis = '';
    this.perangkat.merk = '';
    this.perangkat.type = '';
    this.perangkat.prosesor = '';
    this.perangkat.os = '';
    this.perangkat.ram = 0;
    this.perangkat.hdd = 0;
    this.perangkat.vga = '';
    this.perangkat.monitor = '';
    this.perangkat.antarmuka = '';
    this.perangkat.mac_address = '';
    this.perangkat.tahun_pembuatan = 0;
    this.perangkat.garansi = '';
    this.perangkat.status_perangkat = '';
    this.perangkat.tahun_awal_operasi = 0;
    this.perangkat.idOwner = -1;
    this.perangkat.idPengguna = -1;
    this.perangkat.idCluster = -1;
  }
  initPenguna() {
    this.pegawai.id = -1;
    this.pegawai.nama = '';
    this.pegawai.nip = '';
    this.pegawai.email = '';
    this.pegawai.deskripsi = '';
    this.pegawai.bidang = -1;
  }

  initDataBidang() {
    this.bidangService.getAllData().subscribe(
      response => {
        this.bidangs = response;

        this.dtOptionsBidang = {
          pagingType: 'full_numbers'
        };
      }
    );
  }
  initDataPengelola() {
    this.pengelolaService.getAllData().subscribe(
      response => {
        this.pengelolas = response;

        this.dtOptionsPengelola = {
          pagingType: 'full_numbers'
        };
      }
    );
  }
  initDataKluster() {
    this.kluster.id = -1;
    this.kluster.status = '-';
    this.kluster.curent_cluster = 0;
    this.kluster.recomended_cluster = 0;
  }

  deleteData() {
    this.statusProses.Status = false;
   this.klusterService.deleteDataCluster(this.perangkat.idCluster).subscribe(
        response => {
          console.log('cluster berhasil dihapus');
          // console.log(response);
          this.service.deleteData(this.perangkat.idPengguna).subscribe(
            response =>{
              console.log('Data Pegawai Dihapus');
              // console.log(response);
              this.perangkatService.deleteData(this.perangkat.id).subscribe(
                response =>{
                console.log('Perangkat Berhasil dihapus')
                // console.log(response);
                this.route.navigate(['data-pegawai']);
                }
              );
            }
          );
        }
   );

  }
  updateData() {
    // console.log('DATA RETRIVE Before:');
    // console.log(this.perangkat);
    // console.log(this.pegawai);
    // console.log(this.kluster);


    if (this.validationPegawai(this.pegawai) && this.validationPerangkat(this.perangkat)) {
      // Create data Cluster 
      // console.log('Data Valid true')

      // update Data pegawai
      this.service.updateData(this.id,this.pegawai).subscribe(
        response => {
          console.log('update pergawai done');
          // console.log(response);
        }
      );
      // update Perangkat
      this.perangkatService.updateData(this.perangkat.id, this.perangkat).subscribe(
        response => {
          //  console.log(' update prangkat done');
          //  console.log(response);
           this.route.navigate(['data-pegawai']);

        }
      );


    } else {
      this.addFail = true;
      // console.log('DATA inValid');/
      // console.log(this.invalidMessage);
    }



  }
  addData() {
    // console.log('DATA RETRIVE Before:');
    // console.log(this.perangkat);
    // console.log(this.pegawai);
    // console.log(this.kluster)
    // let idCluster = new Klasifikasi();
    // let idPegawai : Number;
    let idPerangkat: number;


    if (this.validationPegawai(this.pegawai) && this.validationPerangkat(this.perangkat)) {
      this.perangkatService.addData(this.perangkat).subscribe(
        response => {
          // console.log(response);/
          this.perangkat = response;
          
          this.pegawai.id = response.id;
          this.service.addData(this.pegawai).subscribe(
            response => {
              // console.log(response);
              this.kluster.id = response.id;
              this.klusterService.addDataCluster(this.kluster).subscribe(
                  response =>{
                    this.perangkat.idCluster = response.id;
                    this.perangkat.idPengguna = response.id;
                    // console.log(response)
                    this.perangkatService.updateData(this.perangkat.id, this.perangkat).subscribe(
                      response =>{
                        // console.log(response);
                        this.route.navigate(['data-pegawai']);
                      }
                    );
                  }
              );
            }
          );
        }
      );
              

        
    } else {
      this.addFail = true;
      // console.log('DATA inValid');
      // console.log(this.invalidMessage);
    }


  }

  validationPegawai(pegawai: Pegawai) {
    if (pegawai.nama === '') {
      this.invalidMessage = 'Nama Pegawai Tidak Boleh Kosong';
      return false;
    } else if (pegawai.bidang === -1) {
      this.invalidMessage = 'Bidang Tidak Boleh Kosong';
      return false;
    } else if (pegawai.email === '') {
      this.invalidMessage = 'Email Pegawai Tidak Boleh Kosong';
      return false;
    } else {
      return true;
    }
  }

  validationPerangkat(perangkat: Perangkat) {
    if (perangkat.jenis === '') {
      this.invalidMessage = 'Jenis Perangkat Tidak Boleh Kosong';
      return false;
    } else if (perangkat.merk === '') {
      this.invalidMessage = 'Merk Perangkat Tidak Boleh Kosong';
      return false;
    } else if (perangkat.type === '') {
      this.invalidMessage = 'Type Perangkat Tidak Boleh Kosong';
      return false;
    } else if (perangkat.prosesor === '') {
      this.invalidMessage = 'OS Perangkat Tidak Boleh Kosong';
      return false;
    } else if (perangkat.ram <= 0) {
      this.invalidMessage = 'RAM Perangkat Tidak Valid';
      return false;
    } else if (perangkat.hdd <= 0) {
      this.invalidMessage = 'HDD Perangkat Tidak Valid';
      return false;
    } else if (perangkat.vga === '') {
      this.invalidMessage = 'VGA Perangkat Tidak Boleh Kosong';
      return false;
    } else if (perangkat.monitor === '') {
      this.invalidMessage = 'Monitor Perangkat Tidak Boleh Kosong';
      return false;
    } else if (perangkat.antarmuka === '') {
      this.invalidMessage = 'Antarmuka Perangkat Tidak Boleh Kosong';
      return false;
    } else if (perangkat.tahun_pembuatan <= 2000) {
      this.invalidMessage = 'Tahun Pembuatan Tidak Valid';
      return false;
    } else if (perangkat.garansi === '') {
      this.invalidMessage = 'Garansi Perangkat Tidak Boleh Kosong';
      return false;
    } else if (perangkat.status_perangkat === '') {
      this.invalidMessage = 'Status Perangkat Tidak Boleh Kosong';
      return false;
    } else if (perangkat.tahun_awal_operasi <= 2000) {
      this.invalidMessage = 'Tahun Operasi Tidak Valid';
      return false;
    } else if (perangkat.idOwner <= 0) {
      this.invalidMessage = 'Pengelola tidak Boleh Kosong';
      return false;
    } else {
      return true;
    }
  }




  showHideAset() {
    if (this.showPerangkat) {
      this.perangkatBtn = 'btn-success';
      this.perangkatTitle = 'Show';
      this.showPerangkat = false;
    } else {
      this.perangkatBtn = 'btn-primary';
      this.perangkatTitle = 'Hide';

      this.showPerangkat = true;
    }
  }
  showHidePengelola() {
    if (this.showPengelola) {
      this.pengelolaBtn = 'btn-success';
      this.pengelolaTitle = 'Show';
      this.showPengelola = false;
    } else {
      this.pengelolaBtn = 'btn-primary';
      this.pengelolaTitle = 'Hide';

      this.showPengelola = true;
    }
  }

  back() {
    this.route.navigate(['data-pegawai']);
  }
  showBidang() {

    this.selectBidang = true;
  }
  showOwner() {
    this.selectPengelola = true;
  }

  selectBidangId(idBidang: number, bidang) {
    // console.log('id Bidang :' + idBidang + ' / bidang = ' + bidang)
    this.pegawai.bidang = idBidang;
    this.selectBidang = false;
    this.bidangName = bidang;
  }
  selectPengelolaId(id, owner, pengelola) {
    // console.log('id  :' + id + ' / owner = ' + owner + ' / pengelola = ' + pengelola)
    this.perangkat.idOwner = id;
    this.selectPengelola = false;
    this.owner = owner;
    this.pengelola = pengelola;
  }

}
