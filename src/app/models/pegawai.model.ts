import { logging } from "protractor";

export class Pegawai {

    id: number;
    nama: string;
    nip: string;
    email: string;
    deskripsi: string;
    bidang: number
  }