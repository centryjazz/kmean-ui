
export class Perangkat {
    id : number;
    jenis : string;
    merk : string;
    type : string;
    prosesor : string;
    os : string;
    ram :  number;
    hdd :  number;
    vga : string;
    monitor : string;
    antarmuka : string;
    mac_address : string;
    tahun_pembuatan : number;
    garansi : string;
    status_perangkat : string;
    tahun_awal_operasi : number;
    idOwner : number;
    idPengguna : number;
    idCluster : number;

}

