export class KlusterSpec { 
    id:number;
    klasifikasi: number;
    category: string;
    os: string;
    prosesor: string;
    ram: number;
    hdd: number;
    monitor: string;
    wifi: string;
    antarmuka: string;
    mouse: string;
    keyboard: string;
    garansi: string;

}
